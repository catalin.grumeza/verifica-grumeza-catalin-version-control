# **VERSION CONTROL CHEATSHEET**
## Informzaioni base di cos'è il versionamento

 **Repository** 

 E' l'archivio del codice,può essere locale o remoto 

 

**Push** 

Inviare delle modifiche al repository 

 

**Fetch/pull** 

Riceve delle modifiche dal repository 

git fetch è il comando che copia in modo sicuro che controlla i file e vede se ci sono state modifiche senza committare

git pull è il comando che copia dalla repository senza controllare e incolla tutto e committa

 

**È centralizzato** 

Una struttura centralizzata permette di sincronizzare l'accesso al repository. Concretamente impedisce che qualcuno modifichi il repository se nel mentre sono state fatte modifiche dagli altri. 

 

**È distribuito** 

Diversi sviluppatori hanno la possibilità di creare una copia completa del repository ed operare i cambiamenti che desiderano sulla loro copia. quando lo ritengono necessario possono chiedere al proprietario del repository principale di apportare i loro cambiamenti 

 

**Branching model** 

Permette ed incoraggia la creazione di diverse versioni che possono essere indipendenti tra loro, per navigare/eliminare le versioni bastano pochi secondi. 

NOMENCLADUTA DEI BRANCH
E' meglio usare una nomeclatura del tipo /feat/+descrizione del cambiamento 
ex
/feat/debug
/feat/hotfix
/feat/newfeature

## **Base Commands**
* git config --global user.grumeza "Catalin Grumeza" 
--> da il nome dell'user a livello globale che farà i commit 

 

* git config --global user.mail catalin.grumeza@edu.itspiemonte.it 
-->assegno la mail a livello globale della persona che farà i commit 

 

* Ls – la -->directory nascoste 

 

* Cd -->change directory 

 

* Pwd print working directory 

 

* Cd . --> Rimango nella stessa cartalla 

 

* Cd .. --> Torno nella cartella prima 

 

* Mkdir --> crea directory 

 

* Mkdir –p nomecartella1/nomecartella2/nomecartella3 -->crea più cartelle 

## **Comandi di Git**
### **In Git si preferisce lavorare dalla cartella root quindi quando creo un file in una cartella dentro la cartella root scrivo**
* Git init --> inizializza una repository vuota 

 

* Cat -->stampa cose 

 

* Cat .git/config --> stampa il contenuto del file config che si trova in .git

* cat .git/HEAD 

 

* cat .git/refs/heads/master --> prendi last commit 
 

* Touch+nome file --> crea un file ex. Touch README.md 

 

* Git status --> fa vedere lo stato in cui si trovano i file(git status è bene sempre darlo perché non altera niente e non fa danni) 

 

* Git add file.txt --> aggiunge ai file che possono essere poi committati 

* Git add . -->  fa il comando add a tutta la directory 
 
* Git Commit --> registrare, archiviare un pacchetto di informazioni,registra modifiche (ossia "accetta le modifiche e le applica") e commenta aggiungendo –m alla fine e il commento tra "" 

 

* Code + nome file --> modifica da vs code

 

* Nano + nome file --> modifica da shell  

 

* Git rm + nome file -->  rimuove file  

 

* Rm--> fa la stessa cosa di git rm ma poi devo anche fare git add 

 

* Mv + nome del file+nome nuovo -->     lo rinomina,si usa anche per spostare,la rinomina in fin dei conti è uno spostamento

 

* Touch nome cartella/nomefile  -->  creo un file e rimango nella cartella root

* Git tag 1.0.0 --> da etichetta alla storia per dare la versione 

 

* Git stash --> salva i file che vedi in rosso in git status(non addati) e li mette da parte 

 

* Git stash pop --> per farli uscire dalla directory dove sono stati salvati 

 

* Git comment –amend --> modifica descrizione commit 

 

* git rm --cached file.md  --> rimuovi file dall'add  

* Touch .gitignore --> per creare un file dove inserisco le directory/file che non voglio committare e che quindi non apparirano in rosso in git status,serve per file come i log e altri file che non voglio caricare su gitlab(edito il file e aggiungo le directory), 

    Ex:  

        Log 

        Pippo.md 

        Src/*.pyc (ignoro i file che sono nella cartella src che finiscono con ".pyc") 

        !log/config.log (se ho ignorato la cartella ma ho bisogno che un file di quella cartella sia condiviso metto un "!" Davanti al path del file) 

### **Comandi legati alla storia dei commit (LOG e BRANCH)**
* Git log --> mostra i log dei commit
* Git log --pretty=oneline -->  i log compattati 

* git log --pretty=oneline --all –graph -->fa vedere i log dei commit in maniera compatta e anche il grafico dei branch  
* Git show + codice commit --> fa vedere i cambiamenti del file 

 * Git diff--> differenza 2 commit 
 

* Git checkout –b + nome --> creo nuovo branch,il branch è una biforcazione da dove ci troviamo,serve per evitare di modificare lo stato in cui ci troviamo del programma per evitare danni e quindi creiamo questa "safe zone" dove possiamo fare tutto ciò che vogliamo senza arrecare danno al programma in se



* Git merge + nome del branch --> unisce il branch col nome indicato al branch in cui ci troviamo aggiornando così il branch in cui ci troviamo(di solito il master/main). ATTENZIONE,il merge può creare conflitti
* Git rebase --> è un merge per quando devo mergiare un branch partito da un main datato 
### **MAI FARE MERGE SUL MAIN,FAI DEI BRANCH**
### *Conflitti*
* Conflitti: i conflitti capitano quando si mergiano due branch e c'è lo stesso file in entrambi i branch con righe diverse tra loro, per eliminare il conflitto basta che cancello le righe da uno dei due file oppure aprire con vs code e scegliere i file che voglio usare  
dopo aver risolto i conflitti con un editor usare il comando **git merge --continue** per riprendere il merge interrotto dal conflitto
# EVITA COME LA MORTE I CONFLITTI
 ## **Collegamento a Git**
 * ssh-keygen -t ed25519 --> crea keygen con chiavi molto corte a curve ellittiche 

 

* ssh -T git@gitlab.com --> per connetteri al server gitlab 

 

* Git push origin --> pusha i commit locali su gitlab 

* Clone + chiave ssh --> copiamo la chiave ssh di un progetto per accedervi e poterlo modificare
 

* Git remote add origin + link ssh --> entri nel remote server  

* Git push –f fork feat/branch  --> per forzare un push (fork è l'operazione che si fa per modificare un repository di qualcun'altro senza avere permessi per il progetto,serve per averne una copia e lavorarci da solo/in team senza mandare le richieste di merge direttamente al creatore del progetto, "ne diventiamo noi il proprietario "e gestiamo il prgetto)

